module jakchirak {
	requires javafx.controls;
	requires javafx.fxml;
	requires transitive javafx.graphics;
	
	opens org.openjfx to javafx.fxml;
	exports org.openjfx;
	opens org.openjfx.controllers to javafx.fxml;
	exports org.openjfx.controllers;
}